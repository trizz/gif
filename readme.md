## trizzGif

Keep a catalog of gif (or any other) images for quick use. No longer search Google or Giphy for that one 
specific gif, but get it quickly from your own catalog.

### How to install
- Clone this repo
- Copy `.env.example` to `.env`
- Update the `.env` with the correct settings
- Run `composer install`
- Run `php artisan migrate`
- Ready!

**Please note** this is a little personal side project just for fun and trying
 new things. My focus lies on quick results instead of doing everything "the
 perfect way". So yes, I'm using jQuery instead of Vue or React and no, I'm not writing 
 full docblocks or using fancy design patterns. If it bothers you, every 
 contribution is welcome :-)