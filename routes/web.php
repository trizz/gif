<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

// Only access images created by the logged in user.
Route::bind('image', function ($value) {
    return App\Models\Image::where('id', $value)->where('user_id', Auth::user()->id)->first();
});

Route::bind('imageHash', function ($value) {
    // Strip the extension.
    if (strpos($value, '.') !== false) {
        $value = substr($value, 0, strpos($value, '.'));
    }

    return App\Models\Image::where('hash', $value)->first();
});

Route::group(['middleware' => 'auth'], function () {
    Route::get('/', 'ImageController@index');
    Route::get('/image/add', 'ImageController@add');
    Route::post('/image/add', 'ImageController@store');
    Route::post('/image/upload', 'ImageController@upload');
    Route::get('/image/detail/{image}', 'ImageController@detail');
    Route::post('image/tags/{image}', 'ImageController@saveTags');
    Route::get('image/delete/{image}', 'ImageController@delete');
    Route::get('search', 'SearchController@image');

    Route::get('account/tokens/slack', 'AccountController@getSlackToken');
    Route::post('account/tokens/slack', 'AccountController@addSlackToken');
});
Route::get('/slack/oauth', 'SlackAuthController@postOauthTokens');
Route::get('/{imageHash}', 'ImageController@viewRaw');