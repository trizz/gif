<?php

namespace App\Models;

use Cartalyst\Tags\TaggableInterface;
use Cartalyst\Tags\TaggableTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Scout\Searchable;

class Image extends Model implements TaggableInterface
{
    use SoftDeletes, TaggableTrait, Searchable;

    protected $fillable = [
        'user_id',
        'hash',
        'path',
        'mimeType',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        $array = $this->toArray();
        $array['tags'] = $this->tags->toArray();

        return $array;
    }

    /**
     * Get the index name for the model.
     *
     * @return string
     */
    public function searchableAs()
    {
        if (env('IMAGE_INDEX') === null) {
            return config('scout.prefix').$this->getTable();
        }

        return config('scout.prefix').env('IMAGE_INDEX');
    }
}
