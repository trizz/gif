<?php

namespace App\Console\Commands;

use App\Models\Image;
use Illuminate\Console\Command;
use Storage;

class UpdateMimeTypes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:mimetype';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update the mimetype of all images.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Image::all()->each(function (Image $image) {
            $image->mimeType = Storage::mimeType($image->path);
            $image->save();
        });
    }
}
