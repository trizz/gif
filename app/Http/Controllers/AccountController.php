<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddNewSlackToken;
use App\Models\SlackUser;
use App\Models\User;
use Auth;
use Request;
use Response;

class AccountController extends Controller
{
    public function getSlackToken()
    {
        $user = User::find(Auth::id());

        if (Request::get('delete')) {
            $foundTokens = $user->slack->where('id', Request::get('delete'));
            if ($foundTokens->count() === 1) {
                $foundTokens->first()->delete();
            }

            return redirect('account/tokens/slack');
        }

        return view('account.slackToken')->with('tokens', $user->slack);
    }

    public function addSlackToken(AddNewSlackToken $request)
    {
        $user = User::find(Auth::id());
        $slackUser = new SlackUser();
        $slackUser->description = Request::get('token_description');
        $slackUser->user_auth_token = 'tg-'.str_random(37);
        $user->slack()->save($slackUser);

        return redirect('/account/tokens/slack');
    }
}
