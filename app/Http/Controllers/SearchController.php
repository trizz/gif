<?php

namespace App\Http\Controllers;

use App\Models\Image;
use Auth;
use Request;

class SearchController extends Controller
{
    public function image()
    {
        $keyword = Request::get('q');

        if ($keyword === null) {
            return redirect('/');
        }

        $results = Image::search($keyword)->where('user_id', Auth::id())->get();

        return view('image.search')->with('images', $results)->with('keyword', $keyword);
    }
}
