<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Request;
use Response;

class SlackAuthController extends Controller
{
    public function postOauthTokens()
    {
        $client = new Client();

        $result = $client->post(
            'https://slack.com/api/oauth.access',
            [
                'form_params' => [
                    'client_id' => '',
                    'client_secret' => '',
                    'code' => Request::get('code')
                ]
            ]
        );

        if (json_decode($result->getBody()->getContents())->ok === true) {
            return redirect('/account/tokens/slack');
        }

        return Response::make('Can not get a Slack token.');
    }
}
