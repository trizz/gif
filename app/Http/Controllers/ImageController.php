<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddNewImage;
use App\Http\Requests\UploadNewImage;
use App\Models\Image;
use Auth;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use Response;
use Storage;

class ImageController extends Controller
{
    private $validMimeTypes = [
        'image/jpeg',
        'image/jpg',
        'image/png',
        'image/gif',
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $images = Image::where('user_id', Auth::user()->id)->get();

        return view('image.index')->with('images', $images);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
        return view('image.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddNewImage $requestValidator, Request $request)
    {
        $fileName = $request->get('image_url');
        $tmp = tempnam(sys_get_temp_dir(), 'tgif');
        file_put_contents($tmp, file_get_contents($fileName));

        $file = new File($tmp);
        $storedImage = $this->saveFile($file);

        unlink($tmp);

        return redirect('image/detail/'.$storedImage->id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function upload(UploadNewImage $requestValidator, Request $request)
    {

        $file = $request->file('image_upload');

        $storedImage = $this->saveFile($file);

        return redirect('image/detail/'.$storedImage->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function detail(Image $image)
    {
        return view('image.detail')->with('image', $image);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function destroy(Image $image)
    {
        $image->delete();

        return redirect('/');
    }

    /**
     * Output the file to the browser. Use the 'stream' functions to prevent huge memory loads and slow code.
     *
     * @param \App\Image $imageHash
     *
     * @return \Symfony\Component\HttpFoundation\StreamedResponse The streamed response.
     */
    public function viewRaw(Image $imageHash)
    {
        return Response::stream(function() use ($imageHash) {
            fpassthru(Storage::readStream($imageHash->path));
        }, 200, [
            'Content-Type' => Storage::mimeType($imageHash->path),
            'Cache-Control' => 'public, max-age=604800'
        ]);
    }

    /**
     * Save tags for an image.
     *
     * @param \App\Image $image
     *
     * @return \Illuminate\Http\Response
     */
    public function saveTags(Image $image)
    {
        if($image->setTags(\Request::get('tags'))) {
            // When tags are added, force a search update.
            Image::find($image->id)->searchable();
        }

        return Response::make(null, 204);
    }

    /**
     * Delete an image.
     *
     * @param \App\Image $image
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector Redirect back to index.
     */
    public function delete(Image $image)
    {
        $image->delete();

        return redirect('/');
    }

    /**
     * Save the file.
     *
     * @param $file
     *
     * @return Image The saved image.
     *
     * @throws \Exception When the image can not be saved.
     */
    private function saveFile($file)
    {
        if (in_array($file->getMimeType(), $this->validMimeTypes)) {
            $fileHash = md5_file($file->path());
            $storedFile = Storage::putFileAs('images', $file, $fileHash);
            $storedImage = Image::firstOrCreate(
                [
                    'user_id' => Auth::user()->id,
                    'hash' => md5($fileHash.Auth::user()->id),
                    'mimeType' => $file->getMimeType(),
                    'path' => $storedFile,
                ]
            );

            return $storedImage;
        }

        throw new \Exception('Unable to save the image.');
    }
}
