<?php

namespace App\Http\Middleware;

use Closure;
use Config;
use Redirect;
use Request;

class RedirectSsl
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Request::secure() && Config::get('app.debug') === false) {
            return Redirect::secure(Request::path());
        }

        return $next($request);
    }
}
