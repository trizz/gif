<?php
namespace App\SlackHandlers;

use App\Models\SlackUser;
use Spatie\SlashCommand\Attachment;
use Spatie\SlashCommand\Request;
use Spatie\SlashCommand\Response;
use Spatie\SlashCommand\Handlers\BaseHandler;

class RegisterAuthToken extends BaseHandler
{
    /**
     * If this function returns true, the handle method will get called.
     *
     * @param \Spatie\SlashCommand\Request $request
     *
     * @return bool
     */
    public function canHandle(Request $request): bool
    {
        return strlen($request->get('text')) === 40 && strpos($request->get('text'), 'tg-') === 0;
    }

    /**
     * Handle the given request.
     *
     * @param \Spatie\SlashCommand\Request $request
     *
     * @return \Spatie\SlashCommand\Response
     */
    public function handle(Request $request): Response
    {
        $slackUser = SlackUser::where('user_auth_token', $request->text)->first();

        if ($slackUser === null) {
            return $this->respondToSlack('')->withAttachment(
                (new Attachment())
                    ->setTitle('Invalid auth token!')
                    ->setText(sprintf('Generate (a new) one at %s.', \URL::to('account/tokens/slack')))
                    ->setColor('danger')
            );
        }

        $slackUser->slack_user_id = $request->userId;
        $slackUser->slack_team_id = $request->teamId;
        $slackUser->confirmed = true;
        $slackUser->save();

        return $this->respondToSlack('')->withAttachment(
            (new Attachment())
                ->setTitle('Valid auth token!')
                ->setText(sprintf('Welcome %s.', $slackUser->user->name))
                ->setColor('good')
        );

    }
}