<?php
namespace App\SlackHandlers;

use App\Models\SlackUser;
use Spatie\SlashCommand\Attachment;
use Spatie\SlashCommand\Request;
use Spatie\SlashCommand\Response;
use Spatie\SlashCommand\Handlers\BaseHandler;
use URL;

class CatchAll extends BaseHandler
{
    /**
     * If this function returns true, the handle method will get called.
     *
     * @param \Spatie\SlashCommand\Request $request
     *
     * @return bool
     */
    public function canHandle(Request $request): bool
    {
        return true;
    }

    /**
     * Handle the given request.
     *
     * @param \Spatie\SlashCommand\Request $request
     *
     * @return \Spatie\SlashCommand\Response
     */
    public function handle(Request $request): Response
    {
        $link = new Attachment();
        $link->setTitle('Generate a token and authorize to use your gifs.');
        $link->setTitleLink(URL::to('account/tokens/slack'));

        return $this->respondToSlack('')->withAttachment(
            (new Attachment())
                ->setTitle('You are unauthorized or you have deleted your token.')
                ->setText(sprintf('Generate (a new) one at %s.', \URL::to('account/tokens/slack')))
                ->setColor('danger')
        );
    }
}