<?php
namespace App\SlackHandlers;

use App\Models\Image;
use App\Models\SlackUser;
use Spatie\SlashCommand\Attachment;
use Spatie\SlashCommand\Request;
use Spatie\SlashCommand\Response;
use Spatie\SlashCommand\Handlers\BaseHandler;
use URL;

class ImageSearch extends BaseHandler
{
    /**
     * If this function returns true, the handle method will get called.
     *
     * @param \Spatie\SlashCommand\Request $request
     *
     * @return bool
     */
    public function canHandle(Request $request): bool
    {
        return SlackUser::where('slack_user_id', $request->userId)
            ->where('slack_team_id', $request->teamId)
            ->where('confirmed', true)
            ->exists();
    }

    /**
     * Handle the given request.
     *
     * @param \Spatie\SlashCommand\Request $request
     *
     * @return \Spatie\SlashCommand\Response
     */
    public function handle(Request $request): Response
    {
        $slackUser = SlackUser::where('slack_user_id', $request->userId)->where('slack_team_id', $request->teamId)->get();
        if ($slackUser->isEmpty()) {
            return $this->respondToSlack('Whoops... something went wrong! Seems there is no valid user link...');
        }

        $images = Image::search($request->text)->where('user_id', $slackUser->first()->user->id)->get();
        //$images = Image::where('user_id', Auth::id())->get();

        if ($images->isEmpty()) {
            return $this->respondToSlack(
                sprintf('Sorry! Seems you have nothing with a tag `%s`...', $request->text)
            );
        }

        $image = $images->random(1)->first();

        $imageAttachment = new Attachment();
        $imageAttachment->setImageUrl(URL::to($image->hash.'.'.last(explode('/', $image->mimeType))));
        $imageAttachment->setFooter('Powered by trizzGif - '.URL::to(''));

        return $this
            ->respondToSlack('')
            ->displayResponseToEveryoneOnChannel()
            ->withAttachment($imageAttachment);
    }
}