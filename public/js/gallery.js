
jQuery(document).ready(function () {
    $(document).on('click', 'a.ug-tile', function (event) {
        event.preventDefault();

        if (event.altKey) {
            location.href = window.baseUrl+"/image/detail/"+window.hashToId[$(this).attr('href').replace('/', '')];
            return;
        }

        copyToClipboard(window.baseUrl+$(this).attr('href'));
        $(this).fadeOut(150).fadeIn(150);
    });

    var imageList = $('#gallery').unitegallery({
        gallery_theme: 'tiles',
        tiles_type: 'justified',
        tile_enable_action: false,
        tile_as_link: true,
        tile_link_newpage: false,
    });

    if (window.galleryWidth !== '') {
        imageList.resize(window.galleryWidth);
    }
});