@extends('adminlte::page')

@section('title', 'Your Slack token')

@section('content_header')
  <h1>Your Slack token
    <span class="pull-right">
      <a href="https://slack.com/oauth/authorize?&client_id=170317983025.171694220038&scope=commands">
        <img alt="Add to Slack" height="40" width="139" src="https://platform.slack-edge.com/img/add_to_slack.png" srcset="https://platform.slack-edge.com/img/add_to_slack.png 1x, https://platform.slack-edge.com/img/add_to_slack@2x.png 2x" />
      </a>
    </span>
  </h1>
@stop

@section('content')
  <p>
    Below is your Slack token. Use <code>/trizzGif &lt;token&gt;</code> in your Slack team to authorize and connect
    your trizzGif account to slack.
  </p>

  <div class="well well-sm col-md-6">
    @forelse($tokens as $token)
        <b>{{ $token->description ?? 'Slack token' }}:</b> <code>{{ $token->user_auth_token }}</code>
        <a href="#" onclick="copyToClipboard('/trizzgif {{ $token->user_auth_token }}');" title="Copy to clipboard"><i class="fa fa-copy"></i></a>
        <a href="{{ URL::to('account/tokens/slack?delete='.$token->id) }}" title="Delete token"><i class="fa fa-trash"></i></a><br>
    @empty
       No tokens yet!
    @endforelse
  </div>
  <div class="col-md-6">
    <form method="POST">
      {{ csrf_field() }}
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Add a new token</h3>
        </div><!-- /.box-header -->
        <div class="box-body @if($errors->has('token_description')) has-error @endif">
          @if($errors->has('token_description'))
          <div class="alert alert-danger">
              {{ $errors->get('token_description')[0] }}
          </div>
          @endif
          <input type="text" class="form-control" name="token_description" placeholder="Describe the token" />
        </div><!-- /.box-body -->
        <div class="box-footer">
          <button class="btn btn-default pull-right" type="submit">Create new token</button>
        </div><!-- box-footer -->
      </div><!-- /.box -->
    </form>
  </div>
@stop