
@section('css')
    <link rel='stylesheet' href='/plugins/unitegallery/css/unite-gallery.css' type='text/css'/>
@stop

@section('js')
    <script type='text/javascript' src='/plugins/unitegallery/js/unitegallery.min.js'></script>
    <script src='/plugins/unitegallery/themes/tiles/ug-theme-tiles.js' type='text/javascript'></script>
    <script src='/js/gallery.js' type='text/javascript'></script>
    <script>
        window.baseUrl = "{{ URL::to('/') }}";
        window.hashToId = {
            @foreach ($images as $image)
            "{{ $image->hash.'.'.last(explode('/', $image->mimeType)) }}": "{{ $image->id }}",
            @endforeach
        };
        window.galleryWidth = @if($images->count() < 5) '{{ $images->count() * 250 }}' @else '' @endif;
    </script>
@stop

@if ($images->isEmpty())
    <p class="alert alert-info">No images found.</p>
@endif
<div id="gallery" style="display:none;">
    @foreach ($images as $image)
        <a href="/{{ $image->hash.'.'.last(explode('/', $image->mimeType)) }}"
           title="Click to copy URL to your clipboard"
           data-image-id="{{ $image->id }}"
        >
            <img src="/{{ $image->hash.'.'.last(explode('/', $image->mimeType)) }}"
                 alt="Click to copy URL to your clipboard"
                 data-image="/{{ $image->hash.'.'.last(explode('/', $image->mimeType)) }}"
                 data-description="<a href='/image/detail/{{ $image->id }}'>View details</a>"
                 data-image-id="{{ $image->id }}"
            >
        </a>
    @endforeach
</div>
