@extends('adminlte::page')

@section('title', 'Overview')

@section('content_header')
    <h1>Overview of your images<br><small>Click on an image to copy the direct link, or alt+click on an image to go to the details.</small></h1>
@stop

@section('content')
    @include('image.partials.gallery')
@stop