@extends('adminlte::page')

@section('title', 'View image')

@section('css')
    <link rel='stylesheet' href='//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css' type='text/css'/>
@stop

@section('js')
    <script src='//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js' type='text/javascript'></script>
    <script>
        $("#tagList").select2({
            tags: true,
            tokenSeparators: [',', ' '],
            placeholder: 'Enter some tags for this image...'
        }).on('change', function () {
            $.ajax({
                url: '{{ URL::to('image/tags', $image->id) }}',
                type: 'POST',
                data: {
                    tags: $("#tagList").val()
                },
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}',
                },
                dataType: 'json'
            });
        });

        $('#copyLink').on('click', function (e) {
            e.preventDefault();
            copyToClipboard($('#copyLink').attr('href'));
            $($('#copyLink')).fadeOut(150).fadeIn(150);
        })
    </script>
@stop

@section('content_header')
    <h1>View image</h1>
@stop

@section('content')
    <div class="well text-center">
        <img src="/{{ $image->hash.'.'.last(explode('/', $image->mimeType)) }}"><br><br>
        <div class="form-group col-md-6 col-md-offset-3">
            <a href="{{ URL::to($image->hash.'.'.last(explode('/', $image->mimeType))) }}" id="copyLink" class="btn btn-default"><i class="fa fa-copy"></i> Copy link</a>&nbsp;
            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteConfirm">
                <i class="fa fa-trash"></i> Remove image
            </button>
        </div><br>
        <div class="form-group col-md-6 col-md-offset-3">
            <label for="tagList">Tags:</label>
            <select name='tagList' id="tagList" class="form-control" multiple="multiple">
                @foreach ($image->tags as $tag)
                <option selected="selected">{{ $tag->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="modal fade" id="deleteConfirm" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <p>Are you sure you want to delete this image?</p>
                </div>
                <div class="modal-footer">
                    <a href="{{ URL::to('image/delete', $image->id) }}" class="btn btn-danger">Yes, I am sure</a>
                    <button type="button" class="btn btn-default" data-dismiss="modal">No, keep this image</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@stop