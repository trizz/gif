@extends('adminlte::page')

@section('title', 'Search results')

@section('content_header')
    <h1>Search results for "{{ $keyword }}"<br><small>Click on an image to copy the direct link, or alt+click on an image to go to the details.</small></h1>
@stop

@section('content')
    @include('image.partials.gallery')
    <div class="well well-sm text-center col-md-offset-10">
    <p>Search is powered by<br><a href="https://www.algolia.com"><img src="{{ URL::to('images/Algolia_logo_bg-white.jpg') }}" width="100"></a></p>
    </div>
@stop