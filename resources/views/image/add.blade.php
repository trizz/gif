@extends('adminlte::page')

@section('title', 'Add a new image')

@section('content_header')
    <h1>Add a new image</h1>
@stop

@section('content')
    <p>You can either enter an URL to an existing image, or upload a new one.</p>

    <div class="row">
        <div class="col-md-6">
            <form method="POST" action="{{ URL::to('image/add') }}">
                {{ csrf_field() }}
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add by URL</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body @if($errors->has('image_url')) has-error @endif">
                        @if ($errors->has('image_url'))
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->get('image_url') as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <input type="text" class="form-control" name="image_url" placeholder="URL to the image" />
                    </div><!-- /.box-body -->
                    <div class="box-footer">
                        <button class="btn btn-default pull-right" type="submit">Save</button>
                    </div><!-- box-footer -->
                </div><!-- /.box -->
            </form>
        </div>
        <div class="col-md-6">
            <form method="POST" action="{{ URL::to('image/upload') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Upload new</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body @if($errors->has('image_upload')) has-error @endif">
                        @if ($errors->has('image_upload'))
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->get('image_upload') as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <input type="file" name="image_upload">
                    </div><!-- /.box-body -->
                    <div class="box-footer">
                        <button class="btn btn-default pull-right" type="submit">Save</button>
                    </div><!-- box-footer -->
                </div><!-- /.box -->
            </form>
        </div>
    </div>



@stop